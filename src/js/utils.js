export const handleErrors = response => {
    let isError = !response.ok;
    return response.json().then(json => {
        if(isError) {
            throw Error(json.description);
        } else {
            return json;
        }
    });
};

export const formatSortCode = sortCode => {
    if(sortCode) {
        return `${ sortCode.slice(0, 2) }-${ sortCode.slice(2, 4) }-${ sortCode.slice(4) }`;
    }
    return sortCode;
};