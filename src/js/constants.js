export const API = '';

export const CARD_ALGORITHM_TYPES = ["SWITCH", "MODULUS10A", "JOHN_LEWIS", "MS", "MODULUS11"];
export const CARD_SCHEME_NAMES = ["VISA", "MASTERCARD", "AMEX"];
export const CARD_TYPES = ["CREDIT", "DEBIT", "OTHER", "STORE", "UNKNOWN"];
export const COUNTRY_CODES = [
    "BRA",
    "DEU",
    "ITA",
    "PRI",
    "GBR",
    "IDN",
    "ESP",
    "PRT",
    "SGP",
    "NLD",
    "TUR",
    "MYS",
    "LBN",
    "CAN",
    "CZE",
    "KOR",
    "IND",
    "HKG",
    "JPN",
    "AUS",
    "USA",
    "CHE",
    "FRA",
    "SWE"
];