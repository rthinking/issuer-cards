import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

import { reducer as formReducer } from 'redux-form';
import { locale } from './../reducers/locale';
import { auth } from './../reducers/auth';
import { modal } from './../reducers/modal';
import { notifications } from './../reducers/notifications';
import { issuer } from './../reducers/issuer';
import { card } from './../reducers/card';
import { location } from './../reducers/location';

const rootReducer = combineReducers({
	auth,
	locale,
	modal,
	notifications,
	issuer,
	card,
	location,
	routing: routerReducer,
	form: formReducer
});

export const store = compose(
	applyMiddleware(
		thunk,
		routerMiddleware(browserHistory)
	),
	window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)(rootReducer);