import * as actionTypes from './../actionTypes';

export const card = (
    state = {
        id: '',
        name: '',
        questionList: [],
        cardNotificationList: [],
        cardPrefixList: [],
        cardScheme: {}
    },
    action = {}
) => {
    switch(action.type) {
        case actionTypes.CARD_DETAILS_RECEIVED:
            return {
                ...JSON.parse(JSON.stringify(action.card))
            };
        default:
            return state;
    }
};