import * as actionTypes from './../actionTypes';

export const modal = (
    state = {
        isOpened: false,
        component: ''
    },
    action = {}
) => {
    switch(action.type) {
        case actionTypes.MODAL_OPENED:
            return {
                isOpened: true,
                component: action.component,
                props: action.props ? action.props : ''
            };
        case actionTypes.MODAL_CLOSE:
            return {
                isOpened: false,
                component: '',
                props: ''
            };
        default:
            return state;
    }
};