export const locale = (
    state = false,
    action = {}
) => {
    switch (action.type) {
        case 'LOCATION.CHANGE':
            return false;
        default:
            return state;
    }
};