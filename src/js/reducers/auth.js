import * as actionTypes from './../actionTypes';

export const auth = (
    state = {
        isAuthenticated: true,
        userName: 'default@cpp.co.uk'
    },
    action = {}
) => {
    switch(action.type) {
        case actionTypes.LOGIN_RECEIVED:
            return {
                ...state,
                isAuthenticated: true
            };
        case actionTypes.LOGOUT_RECEIVED:
            return {
                ...state,
                isAuthenticated: false
            };
        default:
            return state;
    }
};