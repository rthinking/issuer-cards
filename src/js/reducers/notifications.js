import * as actionTypes from './../actionTypes';
import { LOCATION_CHANGE } from 'react-router-redux';

export const notifications = (
    state = false,
    action = {}
) => {
    switch (action.type) {
        case actionTypes.NOTIFICATION_ADDED:
            return {
                message: action.notificationMessage,
                type: action.notificationType,
                timer: action.timer
            };
        case actionTypes.NOTIFICATION_REMOVED:
            return false;
        case LOCATION_CHANGE:
            clearTimeout(state.timer);
            return false;
        default:
            return state;
    }
};