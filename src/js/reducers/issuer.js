import * as actionTypes from './../actionTypes';

export const issuer = (
    state = {
        id: '',
        issuedCardList: [],
        questionList: [],
        locationList: []
    },
    action = {}
) => {
    switch(action.type) {
        case actionTypes.ISSUER_DETAILS_RECEIVED:
            return {
                ...JSON.parse(JSON.stringify(action.issuer))
            };
        default:
            return state;
    }
};