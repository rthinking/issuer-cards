import * as actionTypes from './../actionTypes';

export const location = (
    state = {
        address: {
            name: ''
        },
        contactPointList: []
    },
    action = {}
) => {
    switch(action.type) {
        case actionTypes.LOCATION_DETAILS_RECEIVED:
            return {
                ...JSON.parse(JSON.stringify(action.location))
            };
        default:
            return state;
    }
};