import styles from './selectWrapper.scss';

import React from 'react';
import Select from 'react-select';

export const SelectWrapper = ({ input, options, placeholder }) => {
    return(
        <Select
            { ...input }
            onChange={
                (e) => {
                    input.onChange(e);
                    input.onBlur(e);
                }
            }
            onBlur={ () => { } }
            placeholder={ placeholder }
            options={ options } />
    );
};