import styles from './contactPoints.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';
import { Input } from './../Input/Input.jsx';

export class ContactPoints extends Component {
    render() {
        const { data, openModal } = this.props;

        const placeholder = <div className={ styles.placeholder }>No contact points found!</div>;

        const locationsView = data.map(currentValue => {
            let data = [
                ['Default contact point', currentValue.defaultContactPoint],
                ['Contact type', currentValue.contactType],
                ['Priority', currentValue.priority],
                ['Phone number', currentValue.phoneNumber],
                ['Phone extension', currentValue.phoneExtension],
                ['Fax number', currentValue.faxNumber],
                ['Monday', currentValue.monday],
                ['Tuesday', currentValue.tuesday],
                ['Wednesday', currentValue.wednesday],
                ['Thursday', currentValue.thursday],
                ['Friday', currentValue.friday],
                ['Saturday', currentValue.saturday],
                ['Sunday', currentValue.sunday],
                ['Bank holiday', currentValue.bankHoliday],
                ['Start time', currentValue.startTime],
                ['End time', currentValue.endTime]
            ];
            return <ListItem
                key={ currentValue.id }
                data={ data }
                handleEditClick={ () => { openModal('EditContactPointForm', { contactPointId: currentValue.id }) } } />;
        });
        return(
            <div>
                <div className={ styles.header }>
                    <div className={ styles.add } onClick={ () => { openModal('NewContactPointForm') }}>Add contact point</div>
                </div>
                {
                    data[0] ? locationsView : placeholder
                }
            </div>
        );
    }
}