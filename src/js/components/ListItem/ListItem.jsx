import styles from './listItem.scss';

import React from 'react';
import classNames from 'classNames';
import { Link } from 'react-router';

export const ListItem = ({ data, name, handleEditClick, urlForTitleLink }) => {
    let nameView, editView;
    if(urlForTitleLink) {
        nameView =
            <Link to={ urlForTitleLink } className={ styles.title }>
                { name }
            </Link>;
    } else {
        if(name) {
            nameView =
                <div className={ styles.notClickableTitle }>
                    { name }
                </div>
        }
    }
    if(handleEditClick) {
        editView =
            <div
                onClick={ handleEditClick }
                className={ styles.button } />;
    }
    return(
        <div className={ styles.wrapper }>
            { nameView }
            <div className={ styles.attributes }>
                {
                    data.map(currentValue => {
                        const isBoolean = (typeof(currentValue[1]) === "boolean");
                        const isNull = currentValue[1] === null;

                        let itemClasses = {};
                        itemClasses[styles.red] = (isBoolean && !currentValue[1]) || isNull;
                        itemClasses[styles.green] = (isBoolean && currentValue[1]);

                        return (
                            <span className={ classNames(itemClasses) } key={ currentValue }>
                                { currentValue[0] }: { isBoolean || isNull ? '' : currentValue[1] + '' }
                            </span>
                        )
                    })
                }
            </div>
            { editView }
        </div>
    );
};