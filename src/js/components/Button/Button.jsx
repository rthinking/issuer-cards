import styles from './button.scss';

import React from 'react';

export const Button = ({ label, disabled, handleClick }) => {
    return(
        <div className={ styles.wrapper }>
            <button
                onClick={ () => { handleClick && handleClick() } }
                disabled={ disabled } >
                { label }
            </button>
        </div>
    );
};