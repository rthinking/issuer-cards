import styles from './card.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';

export const Card = ({ data, openModal }) => {
    const itemData = [
        ['Type', data.cardType],
        ['Loss Notification', data.lossNotification],
        ['Replacement Notification', data.replacementNotification],
        ['Pin Associated', data.pinAssociated],
        ['Obsolete', data.obsolete],
        ['Algorithm Type', data.cardAlgorithmType],
        ['Card scheme', data.cardScheme.schemeName]
    ];
    return(
        <div>
            <ListItem
                data={ itemData }
                handleEditClick={ () => { openModal('EditCardForm', { cardId: data.id, issuerId: data.issuer.id }) } }
            />
        </div>
    );
};