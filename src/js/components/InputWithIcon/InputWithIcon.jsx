import styles from './inputWithIcon.scss';

import React from 'react';

export const InputWithIcon = ({ input, meta, type, style }) => {
    return(
        <div className={ styles.wrapper }>
            <div className={ styles.icon } data-icon={ style } />
            <input type={ type } { ...input } />
        </div>
    );
};