import styles from './tabsWrapper.scss';

import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

Tabs.setUseDefaultStyles(false);

export const TabsWrapper = ({ data }) => {
    return(
        <Tabs forceRenderTabPanel={ true }>
            <TabList className={ styles.tabList }>
                {
                    data.map((currentValue, index) => {
                       return(
                           <Tab key={ index } className={ styles.tab }>
                               { currentValue.title }
                           </Tab>
                       )
                    })
                }
            </TabList>
            {
                data.map((currentValue, index) => {
                    let component;
                    if(currentValue.component) {
                        component = React.createElement(
                            require(`./../${currentValue.component}/${currentValue.component}.jsx`)[currentValue.component],
                            currentValue.componentProps
                        );
                    } else {
                        component = 'placeholder';
                    }
                    return(
                        <TabPanel className={ styles.tabPanel } key={ index }>
                            { component }
                        </TabPanel>
                    )
                })
            }
        </Tabs>
    );
};