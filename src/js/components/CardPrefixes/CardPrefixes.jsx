import styles from './cardPrefixes.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';

export const CardPrefixes = ({ data, openModal }) => {
    const placeholder = <div className={ styles.placeholder }>No card prefixes found!</div>;

    const cardsView = data.map(currentValue => {
        let data = [
            ['Card number length', currentValue.cardNumLength],
            ['Range from', currentValue.rangeFrom],
            ['Range to', currentValue.rangeTo]
        ];
        return <ListItem
            key={ currentValue.id }
            data={ data }
            handleEditClick={ () => { openModal('EditCardPrefixForm', { cardPrefixId: currentValue.id }) } } />;
    });

    return(
        <div>
            <div className={ styles.header }>
                <div className={ styles.add } onClick={ () => { openModal('NewCardPrefixForm') }}>Add new card prefix</div>
            </div>
            {
                data[0] ? cardsView : placeholder
            }
        </div>
    );
};