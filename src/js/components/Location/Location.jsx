import styles from './location.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';
import { formatSortCode } from './../../utils';

export const Location = ({ data, openModal }) => {
    const itemData = [
        ['Country Code', data.countryCode],
        ['Type', data.locationType],
        ['Loss Notification', data.loss],
        ['Replacement Notification', data.replacement],
        ['Obsolete', data.obsolete],
        ['Sort code', formatSortCode(data.sortCode)],
        ['County', data.address.county],
        ['Town', data.address.town],
        ['Post code', data.address.postcode]
    ];
    return(
        <div>
            <ListItem
                data={ itemData }
                handleEditClick={ () => { openModal('EditLocationForm', { locationId: data.id }) } }
            />
        </div>
    );
};