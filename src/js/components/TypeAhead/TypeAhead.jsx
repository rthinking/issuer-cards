import styles from './typeAhead.scss';

import React from 'react';
import Select from 'react-select';

export const TypeAhead = ({ input, placeholder, disabled, getOptions }) => {
    return(
        <Select.Async
            { ...input }
            placeholder={ placeholder }
            loadOptions={ getOptions }
            onChange={
                (e) => {
                    input.onChange(e);
                    input.onBlur(e);
                }
            }
            onBlur={ () => { } }
            disabled={ disabled }
            cache={ false } />
    );
};