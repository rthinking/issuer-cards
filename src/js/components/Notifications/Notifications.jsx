import styles from './notifications.scss';

import React from 'react';

import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export const Notifications = (props) =>
{
    let { data, handleClick } = props;
    let view;
    if(data) {
        view = <div key={ data.message }
                    className={ `${ styles.item } ${ styles[data.type] }` }
        >
            { data.message }
        </div>;
    }
    return (
        <ReactCSSTransitionGroup
            onClick={ () => { handleClick(); } }
            transitionName={{
                enter: styles['enter'],
                enterActive: styles['enterActive'],
                leave: styles['leave'],
                leaveActive: styles['leaveActive']
            }}
            transitionLeaveTimeout={ 0 }
            transitionEnterTimeout={ 0 }
        >
            { view }
        </ReactCSSTransitionGroup>
    );
};