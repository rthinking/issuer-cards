import styles from './textarea.scss';

import React from 'react';

export const Textarea = ({ input, placeholder }) => {
    return(
        <div className={ styles.wrapper }>
            <textarea
                { ...input }
                className={ styles.textarea }
                placeholder={ placeholder } />
        </div>
    );
};