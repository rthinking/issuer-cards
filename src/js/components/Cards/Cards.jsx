import styles from './cards.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';
import { Input } from './../Input/Input.jsx';

export class Cards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ''
        };
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }
    handleFilterChange(e) {
        this.setState({
            filter: e.target.value
        })
    }
    render() {
        const { data, openModal } = this.props;

        const placeholder = <div className={ styles.placeholder }>No issued card found!</div>;

        let filteredList;
        if(this.state.filter) {
            filteredList = data.filter(currentValue => {
                return currentValue.name.search(new RegExp(this.state.filter, 'i')) !== -1;
            });
        } else {
            filteredList = data;
        }

        const cardsView = filteredList.map(currentValue => {
            let data = [
                ['Type', currentValue.cardType],
                ['Loss Notification', currentValue.lossNotification],
                ['Replacement Notification', currentValue.replacementNotification],
                ['Pin Associated', currentValue.pinAssociated],
                ['Obsolete', currentValue.obsolete],
                ['Algorithm Type', currentValue.cardAlgorithmType],
                ['Card scheme', currentValue.cardScheme.schemeName]
            ];
            return <ListItem
                key={ currentValue.id }
                data={ data }
                name={ currentValue.name }
                urlForTitleLink={ `/card?id=${ currentValue.id }` }/>;
        });
        return(
            <div>
                <div className={ styles.header }>
                    <div className={ styles.add } onClick={ () => { openModal('NewCardForm') }}>Add new card</div>
                    <Input
                        handleChange={ this.handleFilterChange }
                        placeholder="Filter by name" />
                </div>
                {
                    filteredList[0] && filteredList[0].name ? cardsView : placeholder
                }
            </div>
        );
    }
}