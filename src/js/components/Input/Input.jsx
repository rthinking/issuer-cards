import styles from './input.scss';

import React from 'react';

export const Input = ({ input, meta, placeholder, handleChange }) => {
    return(
        <div className={ styles.wrapper }>
            <input
                { ...input }
                onChange={
                    (e) => {
                        handleChange ? handleChange(e) : input.onChange(e)
                    }
                }
                className={ styles.input }
                placeholder={ placeholder } />
        </div>
    );
};