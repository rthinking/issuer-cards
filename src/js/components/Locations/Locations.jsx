import styles from './locations.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';
import { Input } from './../Input/Input.jsx';
import { formatSortCode } from './../../utils';

export class Locations extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: ''
        };
        this.handleFilterChange = this.handleFilterChange.bind(this);
    }
    handleFilterChange(e) {
        this.setState({
            filter: e.target.value
        })
    }
    render() {
        const { data, openModal } = this.props;

        const placeholder = <div className={ styles.placeholder }>No locations found!</div>;

        let filteredList;
        if(this.state.filter) {
            filteredList = data.filter(currentValue => {
                return currentValue.sortCode.search(new RegExp(this.state.filter, 'i')) !== -1;
            });
        } else {
            filteredList = data;
        }

        const locationsView = filteredList.map(currentValue => {
            let data = [
                ['Country Code', currentValue.countryCode],
                ['Type', currentValue.locationType],
                ['Loss Notification', currentValue.loss],
                ['Replacement Notification', currentValue.replacement],
                ['Obsolete', currentValue.obsolete],
                ['Sort code', formatSortCode(currentValue.sortCode)]
            ];
            return <ListItem
                key={ currentValue.id }
                data={ data }
                name={ `${ currentValue.address.addressLineOne } ${ currentValue.address.addressLineTwo ? currentValue.address.addressLineTwo : '' }` }
                urlForTitleLink={ `/location?id=${ currentValue.id }` } />;
        });
        return(
            <div>
                <div className={ styles.header }>
                    <div className={ styles.add } onClick={ () => { openModal('NewLocationForm') }}>Add new location</div>
                    <Input
                        handleChange={ this.handleFilterChange }
                        placeholder="Filter by sort code" />
                </div>
                {
                    filteredList[0] && filteredList[0].address ? locationsView : placeholder
                }
            </div>
        );
    }
}