import styles from './layout.scss';

import React from 'react';
import { Link } from 'react-router';

import UserPanel from './../../containers/UserPanel/UserPanel.jsx';
import { Button } from './../Button/Button.jsx';

export const Layout = ({ children, pathname }) => {
    let backButton =
        <Link to="/" className={ styles.back }>
            <Button label="Back to home" />
        </Link>;
    if(pathname === '/') {
        backButton = '';
    }
    return (
        <div className={ styles.wrapper }>
            <div className={ styles.user }>
                <UserPanel />
            </div>
            { backButton }
            { children }
        </div>
    );
};