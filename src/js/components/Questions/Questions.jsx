import styles from './questions.scss';

import React, { Component } from 'react';

import { ListItem } from './../ListItem/ListItem.jsx';

export class Questions extends Component {
    mapDataToList(data, type) {
        const { openModal, id, levelType } = this.props;
        const filteredData = data.filter(currentValue => {
            return currentValue.type === type;
        });
        return filteredData.map(currentValue => {
            return <ListItem
                key={ currentValue.id }
                data={
                    [
                        ['Description', currentValue.description]
                    ]
                }
                handleEditClick={ () => {
                    openModal('EditQuestionForm', {
                        questionId: currentValue.id,
                        id,
                        levelType,
                        levelTypeId: currentValue.questionLevelType.id
                    }) }
                } />
        });
    }
    render() {
        const { data, openModal, id, levelType } = this.props;
        let filteredList = [];
        let locationsView = [];

        const statementPlaceholder = <div className={ styles.placeholder }>No statements found!</div>;
        const statementsView = this.mapDataToList(data, 'STATEMENT');
        const questionsView = this.mapDataToList(data, 'QUESTION');

        return(
            <div>
                <div className={ styles.header }>
                    <div
                        className={ styles.add }
                        onClick={ () => {
                            openModal('NewQuestionForm', { id, levelType })
                        }}>
                            Add new statement/question
                    </div>
                </div>
                <div className={ styles.statementsTitle }>
                    <span>
                        Statements
                    </span>
                </div>
                {
                    statementsView[0] ? statementsView : statementPlaceholder
                }
                <div className={ styles.questionsTitle }>
                    <span>
                        Questions
                    </span>
                </div>
                {
                    questionsView[0] ? questionsView : statementPlaceholder
                }
            </div>
        );
    }
}