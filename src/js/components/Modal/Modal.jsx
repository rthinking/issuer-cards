import style from './modal.scss';

import React, { Component } from 'react';
import classNames from 'classNames';

export class Modal extends Component {
    constructor(props) {
        super(props);
        this.handleEscDown = this.handleEscDown.bind(this);
    }
    componentWillMount() {
        document.addEventListener('keydown', this.handleEscDown);
    }
    componentWillUnmount() {
        document.removeEventListener('keydown', this.handleEscDown);
    }
    handleEscDown(evt) {
        if(this.props.status) {
            evt = evt || window.event;
            var isEscape = false;
            if ("key" in evt) {
                isEscape = (evt.key == "Escape" || evt.key == "Esc");
            } else {
                isEscape = (evt.keyCode == 27);
            }
            if (isEscape) {
                this.props.handleClose();
            }
        }
    }
    render() {
        const { status, component, handleClose, props = {} } = this.props;

        let modalClasses = {};
        modalClasses[style.wrapper] = true;
        modalClasses[style.closed] = !status;

        let form = '';
        if(component) {
            form = React.createElement(require(`./../../containers/${component}/${component}.jsx`).default, props);
        }

        return(
            <div className={ classNames(modalClasses) }>
                <div
                    className={ style.overlay }
                    onClick={ () => { handleClose(); } } />
                <div className={ style.container }>
                    <div
                        className={ style.close }
                        onClick={ () => { handleClose(); } } />
                    { form }
                </div>
            </div>
        );
    }
}