import styles from './editCardPrefixForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';


import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { requestCardPrefixEdit } from './../../actions/CardPrefixActions';

const validate = values => {
    let errors = {};
    if(!values.cardNumLength) {
        errors.cardNumLength = 'Required';
    } else if(values.cardNumLength*1 > 60) {
        errors.cardNumLength = 'Can not be more than 60 chars';
    }
    if(!values.rangeFrom) {
        errors.rangeFrom = 'Required';
    }
    if(!values.rangeTo) {
        errors.rangeTo = 'Required';
    }
    return errors;
};

export const EditCardPrefixForm = ({ pristine, submitting, invalid, handleSubmit, cardPrefixId }) => {
    return(
        <form onSubmit={ handleSubmit((values, dispatch) => {
            dispatch(requestCardPrefixEdit(values, cardPrefixId));
        }) }>
            <div className={ styles.title }>
                <span>
                    Edit the card prefix:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ Input }
                    name="cardNumLength"
                    placeholder="Card number length" />
                <Field
                    component={ Input }
                    name="rangeFrom"
                    placeholder="Range from" />
                <Field
                    component={ Input }
                    name="rangeTo"
                    placeholder="Range to" />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

const editCardPrefixForm = reduxForm({
    form: 'editCardPrefix',
    validate
})(EditCardPrefixForm);

export default connect(
    (state, ownProps) => {
        return {
            initialValues: state.card.cardPrefixList.find(currentValue => {
                return currentValue.id === ownProps.cardPrefixId;
            })
        }
    }
)(editCardPrefixForm);