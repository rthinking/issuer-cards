import styles from './newLocationForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestNewLocation } from './../../actions/LocationActions';
import { COUNTRY_CODES } from './../../constants';

const validate = values => {
    let errors = {};
    if(!values.countryCode) {
        errors.countryCode = 'Required';
    }
    if(!values.locationType) {
        errors.locationType = 'Required';
    }
    if(!values.loss) {
        errors.loss = 'Required';
    }
    if(!values.replacement) {
        errors.replacement = 'Required';
    }
    if(!values.obsolete) {
        errors.obsolete = 'Required';
    }
    if(!values.sortCode) {
        errors.sortCode = 'Required';
    }
    if(!values.addressLineOne) {
        errors.addressLineOne = 'Required';
    }
    if(!values.county) {
        errors.county = 'Required';
    }
    if(!values.town) {
        errors.town = 'Required';
    }
    if(!values.postcode) {
        errors.postcode = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    dispatch(requestNewLocation(values));
};

export const NewLocationForm = ({ pristine, submitting, invalid, handleSubmit }) => {
    return(
        <form onSubmit={ handleSubmit(submit) }>
            <div className={ styles.title }>
                <span>
                    Add a new location:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ SelectWrapper }
                    name="countryCode"
                    placeholder="Country code"
                    options={
                        COUNTRY_CODES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
                <Field
                    component={ SelectWrapper }
                    name="locationType"
                    placeholder="Location type"
                    options={
                        [
                            { label: 'BRANCH', value: 'BRANCH' },
                            { label: 'CENTRAL', value: 'CENTRAL' }
                        ]
                    } />
                <Field
                    component={ SelectWrapper }
                    name="loss"
                    placeholder="Loss Notification"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                <Field
                    component={ SelectWrapper }
                    name="replacement"
                    placeholder="Replacement Notification"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                <Field
                    component={ SelectWrapper }
                    name="obsolete"
                    placeholder="Obsolete"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                <Field
                    component={ Input }
                    name="sortCode"
                    placeholder="Sort code (ex.: 112233)" />
                <br />
                <Field
                    component={ Input }
                    name="addressLineOne"
                    placeholder="Address line 1" />
                <Field
                    component={ Input }
                    name="addressLineTwo"
                    placeholder="Address line 2" />
                <Field
                    component={ Input }
                    name="county"
                    placeholder="County" />
                <Field
                    component={ Input }
                    name="town"
                    placeholder="Town" />
                <Field
                    component={ Input }
                    name="postcode"
                    placeholder="Post code" />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newCard',
    validate
})(NewLocationForm);