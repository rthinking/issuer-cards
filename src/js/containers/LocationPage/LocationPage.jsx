import styles from './locationPage.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestLocationDetails } from './../../actions/LocationActions';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import * as actionTypes from './../../actionTypes';
import { TabsWrapper } from './../../components/TabsWrapper/TabsWrapper.jsx';

class LocationPage extends Component {
    componentWillMount() {
        const { fetchLocation, locationId } = this.props;
        fetchLocation(locationId);
    }
    render() {
        const { location, openModal } = this.props;
        const tabsData = [
            {
                title: `Location`,
                component: 'Location',
                componentProps: {
                    data: location,
                    openModal: openModal
                }
            },
            {
                title: `Contact points (${ location.contactPointList.length })`,
                component: 'ContactPoints',
                componentProps: {
                    data: location.contactPointList,
                    openModal: openModal
                }
            }
        ];

        return (
            <div className={ styles.wrapper }>
                <div className={ styles.title }>
                    { `${ location.address.addressLineOne } ${ location.address.addressLineTwo }` }
                </div>
                <TabsWrapper data={ tabsData } />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    locationId: state.routing.locationBeforeTransitions.query.id,
    location: state.location
});

const mapDispatchToProps = dispatch => ({
    openModal: (component, props) => {
        dispatch({ type: actionTypes.MODAL_OPENED, component, props })
    },
    fetchLocation: id => {
        dispatch(requestLocationDetails(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(LocationPage);