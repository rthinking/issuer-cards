import styles from './newCardPrefixForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { requestNewCardPrefix } from './../../actions/CardPrefixActions';

const validate = values => {
    let errors = {};
    if(!values.cardNumLength) {
        errors.cardNumLength = 'Required';
    } else if(values.cardNumLength*1 > 60) {
        errors.cardNumLength = 'Can not be more than 60 chars';
    }
    if(!values.rangeFrom) {
        errors.rangeFrom = 'Required';
    }
    if(!values.rangeTo) {
        errors.rangeTo = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    dispatch(requestNewCardPrefix(values));
};

export const NewCardPrefix = ({ pristine, submitting, invalid, handleSubmit }) => {
    return(
        <form onSubmit={ handleSubmit(submit) }>
            <div className={ styles.title }>
                <span>
                    Add a new card prefix:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ Input }
                    name="cardNumLength"
                    placeholder="Card number length" />
                <Field
                    component={ Input }
                    name="rangeFrom"
                    placeholder="Range from" />
                <Field
                    component={ Input }
                    name="rangeTo"
                    placeholder="Range to" />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newCardPrefix',
    validate
})(NewCardPrefix);