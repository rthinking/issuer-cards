import styles from './newQuestionForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Textarea } from './../../components/Textarea/Textarea.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestNewQuestion } from './../../actions/QuestionActions';

const validate = values => {
    let errors = {};
    if(!values.description) {
        errors.description = 'Required';
    } else if(values.description.length < 60) {
        errors.description = 'Can not be less than 60 characters';
    }
    if(!values.type) {
        errors.type = 'Required';
    }
    return errors;
};

export const NewQuestionForm = ({ pristine, submitting, invalid, handleSubmit, id, levelType }) => {
    return(
        <form onSubmit={ handleSubmit((values, dispatch) => {
            dispatch(requestNewQuestion(values, id, levelType));
        }) }>
            <div className={ styles.title }>
                <span>
                    Add a new statement/question:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ SelectWrapper }
                    name="type"
                    placeholder="Type"
                    options={
                        [
                            { label: 'QUESTION', value: 'QUESTION' },
                            { label: 'STATEMENT', value: 'STATEMENT' }
                        ]
                    } />
                <Field
                    component={ Textarea }
                    name="description"
                    placeholder="Description" />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newQuestion',
    validate
})(NewQuestionForm);