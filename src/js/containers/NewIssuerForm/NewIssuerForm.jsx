import styles from './newIssuerForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { requestNewIssuer } from './../../actions/IssuerActions';

const submit = (values, dispatch) => {
    dispatch(requestNewIssuer(values.name));
};

export const NewIssuerForm = ({ pristine, submitting, invalid, handleSubmit }) => {
    return(
        <form onSubmit={ handleSubmit(submit) }>
            <div className={ styles.title }>
                <span>
                    Add a new issuer:
                </span>
            </div>
            <Field
                component={ Input }
                name="name"
                placeholder="Issuer name" />
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newIssuer'
})(NewIssuerForm);