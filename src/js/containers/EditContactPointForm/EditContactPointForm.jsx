import styles from './editContactPointForm.scss';

import React from 'react';
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestContactPointEdit } from './../../actions/ContactPointActions';

const validate = values => {
    let errors = {};
    if(!values.defaultContactPoint) {
        errors.defaultContactPoint = 'Required';
    }
    if(!values.defaultContactPoint) {
        errors.defaultContactPoint = 'Required';
    }
    return errors;
};

export const EditContactPointForm = ({ pristine, submitting, invalid, handleSubmit, contactPointId }) => {
    return(
        <form onSubmit={ handleSubmit((values, dispatch) => {
            dispatch(requestContactPointEdit(values, contactPointId));
        }) }>
            <div className={ styles.title }>
                <span>
                    Edit the contact point:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ SelectWrapper }
                    name="defaultContactPoint"
                    placeholder="Default contact point"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                <Field
                    component={ SelectWrapper }
                    name="contactType"
                    placeholder="Contact type"
                    options={
                        [
                            { label: 'LT', value: 'LT' },
                            { label: 'FX', value: 'FX' },
                            { label: 'PH', value: 'PH' }
                        ]
                    } />
                <Field
                    component={ Input }
                    name="priority"
                    placeholder="Priority" />
                <Field
                    component={ Input }
                    name="phoneExtension"
                    placeholder="Phone extension" />
                <Field
                    component={ Input }
                    name="phoneNumber"
                    placeholder="Phone number" />
                <Field
                    component={ Input }
                    name="faxNumber"
                    placeholder="Fax number" />
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="monday"
                        placeholder="Monday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="tuesday"
                        placeholder="Tuesday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="wednesday"
                        placeholder="Wednesday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="thursday"
                        placeholder="Thursday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="friday"
                        placeholder="Friday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="saturday"
                        placeholder="Saturday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="sunday"
                        placeholder="Sunday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="bankHoliday"
                        placeholder="Bank holiday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ Input }
                        name="startTime"
                        placeholder="Start time (HH:mm)" />
                    <Field
                        component={ Input }
                        name="endTime"
                        placeholder="End time (HH:mm)" />
                </div>
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Save" />
            </div>
        </form>
    );
};

const editContactPointForm = reduxForm({
    form: 'editContactPoint',
    validate
})(EditContactPointForm);

export default connect(
    (state, ownProps) => {
        const data = (() => {
            for(let i = 0; i < state.location.contactPointList.length; i++) {
                if(state.location.contactPointList[i].id === ownProps.contactPointId) {
                    let dataObject = JSON.parse(JSON.stringify(state.location.contactPointList[i]));
                    for(let key in dataObject) {
                        if(!dataObject.hasOwnProperty(key)) continue;
                        if(typeof(dataObject[key]) === 'boolean') {
                            dataObject[key] = { label: dataObject[key] ? 'Yes' : 'No', value: dataObject[key] }
                        } else if(key === 'contactType') {
                            dataObject[key] = { label: dataObject[key], value: dataObject[key] }
                        }
                    }
                    return dataObject
                }
            }
        })();
        console.log(data);
        return {
            initialValues: data
        }
    }
)(editContactPointForm);