import styles from './issuedCardPage.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestCardDetails } from './../../actions/CardActions';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import * as actionTypes from './../../actionTypes';
import { IssuedCards } from '../../components/Cards/Cards.jsx';
import { TabsWrapper } from './../../components/TabsWrapper/TabsWrapper.jsx';

class IssuedCardPage extends Component {
    componentWillMount() {
        const { fetchCard, cardId } = this.props;
        fetchCard(cardId);
    }
    render() {
        const { card, openModal } = this.props;

        /*const tabsData = [
            {
                title: `Locations (${ issuer.locationList.length })`,
                component: 'Locations',
                componentProps: {
                    data: issuer.locationList,
                    openModal: openModal
                }
            },
            {
                title: `Issued Cards (${ issuer.issuedCardList.length })`,
                component: 'IssuedCards',
                componentProps: {
                    data: issuer.issuedCardList,
                    openModal: openModal
                }
            },
            {
                title: `Questions (${ issuer.questionList.length })`,
                component: 'Questions',
                componentProps: {
                    data: issuer.questionList,
                    openModal: openModal,
                    id: issuer.id,
                    levelType: 'ISSUER'
                }
            },
        ];*/
        const tabsData = [
            {
                title: `Card`,
                component: 'Card',
                componentProps: {
                    data: card,
                    openModal: openModal
                }
            },
            {
                title: `Card prefixes`,
                component: 'CardPrefixes',
                componentProps: {
                    data: card.cardPrefixList,
                    openModal: openModal
                }
            }
        ];

        return (
            <div className={ styles.wrapper }>
                <div className={ styles.title }>
                    { card.name }
                </div>
                <TabsWrapper data={ tabsData } />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cardId: state.routing.locationBeforeTransitions.query.id,
    card: state.card
});

const mapDispatchToProps = dispatch => ({
    openModal: (component, props) => {
        dispatch({ type: actionTypes.MODAL_OPENED, component, props })
    },
    fetchCard: id => {
        dispatch(requestCardDetails(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(IssuedCardPage);