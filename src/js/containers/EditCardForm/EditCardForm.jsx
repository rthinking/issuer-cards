import styles from './editCardForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { connect } from 'react-redux';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestCardEdit } from './../../actions/CardActions';
import { CARD_ALGORITHM_TYPES, CARD_SCHEME_NAMES, CARD_TYPES } from './../../constants';

const validate = values => {
    let errors = {};
    if(!values.name) {
        errors.name = 'Required';
    }
    if(!values.cardScheme) {
        errors.cardScheme = 'Required';
    }
    if(!values.cardAlgorithmType) {
        errors.cardAlgorithmType = 'Required';
    }
    if(!values.cardType) {
        errors.cardType = 'Required';
    }
    return errors;
};

export const EditCardForm = ({ pristine, submitting, invalid, handleSubmit, cardId, issuerId }) => {
    return(
        <form onSubmit={ handleSubmit((values, dispatch) => {
            dispatch(requestCardEdit(values, cardId, issuerId));
        }) }>
            <div className={ styles.title }>
                <span>
                    Edit the card:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ Input }
                    name="name"
                    placeholder="Card name" />
                <Field
                    component={ SelectWrapper }
                    name="cardScheme"
                    placeholder="Card scheme"
                    options={
                        CARD_SCHEME_NAMES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
                <Field
                    component={ SelectWrapper }
                    name="cardAlgorithmType"
                    placeholder="Algorithm type"
                    options={
                        CARD_ALGORITHM_TYPES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
                <Field
                    component={ SelectWrapper }
                    name="cardType"
                    placeholder="Card type"
                    options={
                        CARD_TYPES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Save" />
            </div>
        </form>
    );
};

const editCardForm = reduxForm({
    form: 'editCard',
    validate
})(EditCardForm);

export default connect(
    state => {
        const data = (() => {
            let dataObject = JSON.parse(JSON.stringify(state.card));
            for(let key in dataObject) {
                if(!dataObject.hasOwnProperty(key)) continue;
                if(key === 'cardScheme') {
                    dataObject[key] = { label: dataObject[key].schemeName, value: dataObject[key].schemeName }
                } else if((key === 'cardAlgorithmType' && dataObject[key]) || key === 'cardType') {
                    dataObject[key] = { label: dataObject[key], value: dataObject[key] }
                }
            }
            return { ...dataObject }
        })();
        return {
            initialValues: data
        }
    }
)(editCardForm);