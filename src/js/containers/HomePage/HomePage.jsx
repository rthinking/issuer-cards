import styles from './homePage.scss';

import React from 'react';
import { connect } from 'react-redux';

import { Button } from './../../components/Button/Button.jsx';
import * as actionTypes from './../../actionTypes';
import InitialSearch from '../InitialSearchForm/InitialSearchForm.jsx';

const Home = ({ openModal }) => {
    return (
        <div className={ styles.wrapper }>
            <div className={ styles.newIssuer }>
                <Button
                    handleClick={ () => { openModal("NewIssuerForm") } }
                    label="New issuer" />
            </div>
            <span className={ styles.separator }>
                OR
            </span>
            <InitialSearch />
        </div>
    );
};

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = dispatch => ({
    openModal: component => {
        dispatch({ type: actionTypes.MODAL_OPENED, component })
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);