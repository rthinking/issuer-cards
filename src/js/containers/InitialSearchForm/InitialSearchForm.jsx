import styles from './initialSearchForm.scss';

import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { push } from 'react-router-redux';
import classNames from 'classNames';

import { TypeAhead } from './../../components/TypeAhead/TypeAhead.jsx';
import { requestIssuerSearch } from './../../actions/IssuerActions';
import { requestCardSearch } from './../../actions/CardActions';
import { Button } from './../../components/Button/Button.jsx';

const validate = values => {
    let errors = {};
    if(!values.issuer && !values.card) {
        errors.issuer = 'Required';
    }
    if(!values.card && !values.issuer) {
        errors.card = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    if(values.issuer) {
        dispatch(push(`/issuer?id=${ values.issuer.value }`));
    } else if(values.card) {
        dispatch(push(`/card?id=${ values.card.value }`));
    }
};

const InitialSearch = ({ formValues, dispatch, pristine, submitting, invalid, handleSubmit }) => {
    const isCardFieldActive = formValues && formValues.values && !!formValues.values.card;
    const isIssuerFieldActive = formValues && formValues.values && !!formValues.values.issuer;

    let issuerFieldClasses = {};
    issuerFieldClasses[styles.issuer] = true;
    issuerFieldClasses[styles.disabled] = isCardFieldActive;
    issuerFieldClasses[styles.active] = isIssuerFieldActive;

    let cardFieldClasses = {};
    cardFieldClasses[styles.card] = true;
    cardFieldClasses[styles.active] = isCardFieldActive;
    cardFieldClasses[styles.disabled] = isIssuerFieldActive;

    return(
        <form className={ styles.wrapper } onSubmit={ handleSubmit(submit) }>
            <div className={ styles.container }>
                <div className={ classNames(issuerFieldClasses) }>
                    <Field
                        component={ TypeAhead }
                        name="issuer"
                        placeholder="Issuer"
                        disabled={ isCardFieldActive }
                        getOptions={
                            value => {
                                return dispatch(requestIssuerSearch(value));
                            }
                        }
                        isLoading={ true } />
                </div>
                <div className={ classNames(cardFieldClasses) }>
                    <Field
                        component={ TypeAhead }
                        name="card"
                        placeholder="Card"
                        disabled={ isIssuerFieldActive }
                        getOptions={
                            value => {
                                return dispatch(requestCardSearch(value));
                            }
                        } />
                </div>
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Go" />
            </div>
        </form>
    );
};

const initialSearch = reduxForm({
    form: 'initialSearch',
    destroyOnUnmount: false,
    validate
})(InitialSearch);

const mapStateToProps = (state) => ({
    formValues: state.form.initialSearch
});

export default connect(mapStateToProps)(initialSearch);