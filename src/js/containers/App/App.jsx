import styles from './app.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import Login from '../LoginForm/LoginForm.jsx';
import { Layout } from './../../components/Layout/Layout.jsx';
import { Modal } from './../../components/Modal/Modal.jsx';
import { Notifications } from './../../components/Notifications/Notifications.jsx';
import * as actionTypes from './../../actionTypes';
import { removeNotification } from './../../actions/NotificationsActions';

class App extends Component {
    render() {
        const {
            children,
            authStatus,
            isModalOpened,
            modalComponent,
            modalComponentProps,
            closeModal,
            notifications,
            removeNotification,
            currentPathname
        } = this.props;

        let mainView =
            <Layout pathname={ currentPathname }>
                { children }
            </Layout>;

        if(!authStatus) {
            mainView = <Login />;
        }

        return (
            <div className={ styles.wrapper }>
                { mainView }
                <Modal
                    status={ isModalOpened }
                    component={ modalComponent }
                    handleClose={ closeModal }
                    props={ modalComponentProps }
                />
                <Notifications data={ notifications } handleClick={ removeNotification } />
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    authStatus: state.auth.isAuthenticated,
    userName: state.auth.userName,
    isModalOpened: state.modal.isOpened,
    modalComponent: state.modal.component,
    modalComponentProps: state.modal.props,
    notifications: state.notifications,
    currentPathname: state.routing.locationBeforeTransitions.pathname
});

const mapDispatchToProps = dispatch => ({
    closeModal: () => {
        dispatch({ type: actionTypes.MODAL_CLOSE })
    },
    removeNotification: () => {
        dispatch(removeNotification());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(App);