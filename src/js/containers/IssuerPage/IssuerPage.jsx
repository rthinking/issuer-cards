import styles from './issuerPage.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestIssuerDetails } from './../../actions/IssuerActions';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import * as actionTypes from './../../actionTypes';
import { IssuedCards } from '../../components/Cards/Cards.jsx';
import { TabsWrapper } from './../../components/TabsWrapper/TabsWrapper.jsx';

class IssuerPage extends Component {
    componentWillMount() {
        const { fetchIssuer, issuerId } = this.props;
        fetchIssuer(issuerId);
    }
    render() {
        const { issuer, openModal } = this.props;

        const tabsData = [
            {
                title: `Locations (${ issuer.locationList.length })`,
                component: 'Locations',
                componentProps: {
                    data: issuer.locationList,
                    openModal: openModal
                }
            },
            {
                title: `Issued Cards (${ issuer.issuedCardList.length })`,
                component: 'Cards',
                componentProps: {
                    data: issuer.issuedCardList,
                    openModal: openModal
                }
            },
            {
                title: `Questions (${ issuer.questionList.length })`,
                component: 'Questions',
                componentProps: {
                    data: issuer.questionList,
                    openModal: openModal,
                    id: issuer.id,
                    levelType: 'ISSUER'
                }
            },
        ];

        return (
            <div className={ styles.wrapper }>
                <div className={ styles.title }>
                    { issuer.name }
                </div>
                <TabsWrapper data={ tabsData } />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    issuerId: state.routing.locationBeforeTransitions.query.id,
    issuer: state.issuer
});

const mapDispatchToProps = dispatch => ({
    openModal: (component, props) => {
        dispatch({ type: actionTypes.MODAL_OPENED, component, props })
    },
    fetchIssuer: id => {
        dispatch(requestIssuerDetails(id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(IssuerPage);