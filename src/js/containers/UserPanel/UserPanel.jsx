import styles from './userPanel.scss';

import React from 'react';
import { connect } from 'react-redux';

import * as actionTypes from './../../actionTypes';

const UserPanel = ({ userName, logout }) => {
    return (
        <div className={ styles.wrapper }>
            <div className={ styles.icon } />
            <div className={ styles.content }>
                { userName }
                <span
                    className={ styles.logout }
                    onClick={ () => { logout(); } } >
                    Logout
                </span>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    userName: state.auth.userName
});

const mapDispatchToProps = dispatch => ({
    logout: () => {
        dispatch({ type: actionTypes.LOGOUT_RECEIVED });
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(UserPanel);