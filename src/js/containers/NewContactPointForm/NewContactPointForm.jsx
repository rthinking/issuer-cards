import styles from './newContactPointForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestNewContactPoint } from './../../actions/ContactPointActions';

const validate = values => {
    let errors = {};
    if(!values.defaultContactPoint) {
        errors.defaultContactPoint = 'Required';
    }
    if(!values.defaultContactPoint) {
        errors.defaultContactPoint = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    dispatch(requestNewContactPoint(values));
};

export const NewContactPointForm = ({ pristine, submitting, invalid, handleSubmit }) => {
    return(
        <form onSubmit={ handleSubmit(submit) }>
            <div className={ styles.title }>
                <span>
                    Add a contact point:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ SelectWrapper }
                    name="defaultContactPoint"
                    placeholder="Default contact point"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                <Field
                    component={ SelectWrapper }
                    name="contactType"
                    placeholder="Contact type"
                    options={
                        [
                            { label: 'LT', value: 'LT' },
                            { label: 'FX', value: 'FX' },
                            { label: 'PH', value: 'PH' }
                        ]
                    } />
                <Field
                    component={ Input }
                    name="priority"
                    placeholder="Priority" />
                <Field
                    component={ Input }
                    name="phoneExtension"
                    placeholder="Phone extension" />
                <Field
                    component={ Input }
                    name="phoneNumber"
                    placeholder="Phone number" />
                <Field
                    component={ Input }
                    name="faxNumber"
                    placeholder="Fax number" />
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="monday"
                        placeholder="Monday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="tuesday"
                        placeholder="Tuesday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="wednesday"
                        placeholder="Wednesday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="thursday"
                        placeholder="Thursday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ SelectWrapper }
                        name="friday"
                        placeholder="Friday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                    <Field
                        component={ SelectWrapper }
                        name="saturday"
                        placeholder="Saturday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                    component={ SelectWrapper }
                    name="sunday"
                    placeholder="Sunday"
                    options={
                        [
                            { label: 'Yes', value: true },
                            { label: 'No', value: false }
                        ]
                    } />
                    <Field
                        component={ SelectWrapper }
                        name="bankHoliday"
                        placeholder="Bank holiday"
                        options={
                            [
                                { label: 'Yes', value: true },
                                { label: 'No', value: false }
                            ]
                        } />
                </div>
                <div className={ styles.group }>
                    <Field
                        component={ Input }
                        name="startTime"
                        placeholder="Start time (HH:mm)" />
                    <Field
                        component={ Input }
                        name="endTime"
                        placeholder="End time (HH:mm)" />
                </div>
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newContactPoint',
    validate
})(NewContactPointForm);