import styles from './loginForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import { InputWithIcon } from './../../components/InputWithIcon/InputWithIcon.jsx';
import { Button } from './../../components/Button/Button.jsx';
import * as actionTypes from './../../actionTypes';

const validate = values => {
    let errors = {};
    if(!values.username) {
        errors.username = 'Required';
    }
    if(!values.password) {
        errors.password = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    dispatch({ type: actionTypes.LOGIN_RECEIVED });
};

const Login = ({ handleSubmit, pristine, submitting, invalid }) => {
    return(
        <div className={ styles.wrapper }>
            <form className={ styles.form } onSubmit={ handleSubmit(submit) }>
                <div className={ styles.title }>
                    <span>
                        CIS Maintenance Forms
                    </span>
                </div>
                <div>
                    <Field
                        component={ InputWithIcon }
                        name="username"
                        type="text"
                        style="user" />
                    <Field
                        component={ InputWithIcon }
                        name="password"
                        type="password"
                        style="lock" />
                </div>
                <div className={ styles.submit }>
                    <Button disabled={ pristine || submitting || invalid } label="Login" />
                </div>
            </form>
        </div>
    );
};

export default reduxForm({
    form: 'login',
    destroyOnUnmount: false,
    validate
})(Login);