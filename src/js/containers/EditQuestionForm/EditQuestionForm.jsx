import styles from './editQuestionForm.scss';

import React from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import { Button } from './../../components/Button/Button.jsx';
import { Textarea } from './../../components/Textarea/Textarea.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestQuestionEdit } from './../../actions/QuestionActions';

const validate = values => {
    let errors = {};
    if(!values.type) {
        errors.type = 'Required';
    }
    if(!values.description) {
        errors.description = 'Required';
    }
    return errors;
};

const EditQuestionForm = ({ pristine, submitting, invalid, handleSubmit, questionId, id, levelType, levelTypeId }) => {
    return(
        <form onSubmit={ handleSubmit((values, dispatch) => {
            dispatch(requestQuestionEdit(values, questionId, id, levelType, levelTypeId));
        }) }>
            <div className={ styles.title }>
                <span>
                    Edit the statement/question:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ SelectWrapper }
                    name="type"
                    placeholder="Type"
                    options={
                        [
                            { label: 'QUESTION', value: 'QUESTION' },
                            { label: 'STATEMENT', value: 'STATEMENT' }
                        ]
                    } />
                <Field
                    component={ Textarea }
                    name="description"
                    placeholder="Description" />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Save" />
            </div>
        </form>
    );
};

const editQuestionForm = reduxForm({
    form: 'editQuestion',
    validate
})(EditQuestionForm);

export default connect(
    (state, ownProps) => {
        const data = (() => {
            for(let i = 0; i < state.issuer.questionList.length; i++) {
                if(state.issuer.questionList[i].id === ownProps.questionId) {
                    console.log(123);
                    let dataObject = JSON.parse(JSON.stringify(state.issuer.questionList[i]));
                    for(let key in dataObject) {
                        if(!dataObject.hasOwnProperty(key)) continue;
                        if(key === 'type') {
                            dataObject[key] = { label: dataObject[key], value: dataObject[key] }
                        }
                    }
                    return {
                        ...dataObject
                    }
                }
            }
        })();
        return {
            initialValues: data
        }
    }
)(editQuestionForm);