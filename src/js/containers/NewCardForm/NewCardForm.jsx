import styles from './newCardForm.scss';

import React from 'react';
import { reduxForm, Field } from 'redux-form';
import Select from 'react-select';

import { Button } from './../../components/Button/Button.jsx';
import { Input } from './../../components/Input/Input.jsx';
import { SelectWrapper } from './../../components/SelectWrapper/SelectWrapper.jsx';
import { requestNewCard } from './../../actions/CardActions';
import { CARD_ALGORITHM_TYPES, CARD_SCHEME_NAMES, CARD_TYPES } from './../../constants';

const validate = values => {
    let errors = {};
    if(!values.name) {
        errors.name = 'Required';
    }
    if(!values.cardScheme) {
        errors.cardScheme = 'Required';
    }
    if(!values.cardAlgorithmType) {
        errors.cardAlgorithmType = 'Required';
    }
    if(!values.cardType) {
        errors.cardType = 'Required';
    }
    return errors;
};

const submit = (values, dispatch) => {
    dispatch(requestNewCard(values));
};

export const NewCardForm = ({ pristine, submitting, invalid, handleSubmit }) => {
    return(
        <form onSubmit={ handleSubmit(submit) }>
            <div className={ styles.title }>
                <span>
                    Add a new card:
                </span>
            </div>
            <div className={ styles.fields }>
                <Field
                    component={ Input }
                    name="name"
                    placeholder="Card name" />
                <Field
                    component={ SelectWrapper }
                    name="cardScheme"
                    placeholder="Card scheme"
                    options={
                        CARD_SCHEME_NAMES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
                <Field
                    component={ SelectWrapper }
                    name="cardAlgorithmType"
                    placeholder="Algorithm type"
                    options={
                        CARD_ALGORITHM_TYPES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
                <Field
                    component={ SelectWrapper }
                    name="cardType"
                    placeholder="Card type"
                    options={
                        CARD_TYPES.map(currentValue => {
                            return { label: currentValue, value: currentValue }
                        })
                    } />
            </div>
            <div className={ styles.submit }>
                <Button
                    disabled={ pristine || submitting || invalid }
                    label="Create" />
            </div>
        </form>
    );
};

export default reduxForm({
    form: 'newCard',
    validate
})(NewCardForm);