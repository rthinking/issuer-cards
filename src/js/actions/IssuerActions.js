import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { addNotification } from './NotificationsActions';
import { handleErrors } from './../utils';

export const requestIssuerSearch = input => (
    dispatch => {
        return fetch(`${ API }/issuer/search?name=${ input }`)
            .then((response) => {
                return response.json();
            }).then((json) => {
                let results = json.issuers;
                if(json.issuers.length > 0) {
                    results = json.issuers.map( currentValue => {
                        return {
                            value: currentValue.id,
                            label: currentValue.name
                        }
                    });
                }
                return { options: results };
            });
    }
);

export const requestNewIssuer = name => (
    dispatch => {
        return fetch(`${ API }/issuer/add`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name })
        }).then(handleErrors).then((json) => {
            dispatch(addNotification(
                'Issuer successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestIssuerDetails = id => (
    dispatch => {
        dispatch({ type: actionTypes.ISSUER_DETAILS_REQUESTED });
        return fetch(`${ API }/issuer/search?id=${id}`,)
            .then(handleErrors).then((json) => {
                dispatch({
                    type: actionTypes.ISSUER_DETAILS_RECEIVED,
                    issuer: json.issuers[0]
                });
            }).catch(error => {
                dispatch(addNotification(
                    error.message,
                    'error'
                ));
            });
    }
);