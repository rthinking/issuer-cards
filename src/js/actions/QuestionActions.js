import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { addNotification } from './NotificationsActions';
import { handleErrors } from './../utils';
import { requestIssuerDetails } from './IssuerActions';

export const requestNewQuestion = (values, id, levelType) => (
    (dispatch, getState) => {
        return fetch(`${ API }/question`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                question: {
                    description: values.description,
                    questionLevelType: {
                        entityID: id,
                        levelType
                    },
                    type: values.type.value
                }
            })
        }).then(handleErrors).then((json) => {
            dispatch(addNotification(
                'Statement/question successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestIssuerDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestQuestionEdit = (values, questionId, id, levelType, levelTypeId) => (
    (dispatch, getState) => {
        return fetch(`${ API }/question/update`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                question: {
                    id: questionId,
                    description: values.description,
                    questionLevelType: {
                        id: levelTypeId,
                        entityID: id,
                        levelType
                    },
                    type: values.type.value
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Question successfully edited!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestIssuerDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);