import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { handleErrors } from './../utils';
import { addNotification } from './NotificationsActions';
import { requestIssuerDetails } from './IssuerActions';

export const requestCardSearch = input => (
    dispatch => {
        return fetch(`${ API }/issuedcard/search?name=${ input }`)
            .then((response) => {
                return response.json();
            }).then((json) => {
                let results = json.issuedCards;
                if(json.issuedCards.length > 0) {
                    results = json.issuedCards.map( currentValue => {
                        return {
                            value: currentValue.id,
                            label: currentValue.name
                        }
                    });
                }
                return { options: results };
            });
    }
);

export const requestNewCard = values => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/issuedcard/add`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: values.name,
                cardAlgorithmType: values.cardAlgorithmType.value,
                cardScheme: {
                    schemeName: values.cardScheme.value
                },
                cardType: values.cardType.value,
                issuer: {
                    id: getState().routing.locationBeforeTransitions.query.id
                }
            })
        }).then(handleErrors).then((json) => {
            dispatch(addNotification(
                'Card successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestIssuerDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            console.log(error);
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestCardDetails = id => (
    dispatch => {
        dispatch({ type: actionTypes.CARD_DETAILS_REQUESTED });
        return fetch(`${ API }/issuedcard/search?id=${id}`,)
            .then(handleErrors).then((json) => {
                dispatch({
                    type: actionTypes.CARD_DETAILS_RECEIVED,
                    card: json.issuedCards[0]
                });
            }).catch(error => {
                dispatch(addNotification(
                    error.message,
                    'error'
                ));
            });
    }
);

export const requestCardEdit = (values, cardId, issuerId) => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/issuedcard/update`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: values.name,
                cardAlgorithmType: values.cardAlgorithmType.value,
                cardScheme: {
                    schemeName: values.cardScheme.value
                },
                cardType: values.cardType.value,
                id: getState().routing.locationBeforeTransitions.query.id,
                issuer: {
                    id: issuerId
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Card successfully edited!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestCardDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);