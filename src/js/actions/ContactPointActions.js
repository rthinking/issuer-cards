import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { handleErrors } from './../utils';
import { addNotification } from './NotificationsActions';
import { requestLocationDetails } from './LocationActions';

export const requestNewContactPoint = values => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CONTACT_POINT_REQUESTED });
        return fetch(`${ API }/contactpoint`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                contactPoint: {
                    bankHoliday: values.bankHoliday && values.bankHoliday.value,
                    contactType: values.contactType.value,
                    defaultContactPoint: values.defaultContactPoint.value,
                    startTime: values.startTime,
                    endTime: values.endTime,
                    faxNumber: values.faxNumber,
                    friday: values.friday && values.friday.value,
                    monday: values.monday && values.monday.value,
                    phoneExtension: values.phoneExtension,
                    phoneNumber: values.phoneNumber,
                    priority: values.priority,
                    saturday: values.saturday && values.saturday.value,
                    sunday: values.sunday && values.sunday.value,
                    thursday: values.thursday && values.thursday.value,
                    tuesday: values.tuesday && values.tuesday.value,
                    wednesday: values.wednesday && values.wednesday.value,
                    locationID: getState().routing.locationBeforeTransitions.query.id
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Contact point successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestLocationDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            console.log(error.message);
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestContactPointEdit = (values, contactPointId) => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/contactpoint/update`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                contactPoint: {
                    bankHoliday: values.bankHoliday && values.bankHoliday.value,
                    contactType: values.contactType.value,
                    defaultContactPoint: values.defaultContactPoint.value,
                    startTime: values.startTime,
                    endTime: values.endTime,
                    faxNumber: values.faxNumber,
                    friday: values.friday && values.friday.value,
                    monday: values.monday && values.monday.value,
                    phoneExtension: values.phoneExtension,
                    phoneNumber: values.phoneNumber,
                    priority: values.priority,
                    saturday: values.saturday && values.saturday.value,
                    sunday: values.sunday && values.sunday.value,
                    thursday: values.thursday && values.thursday.value,
                    tuesday: values.tuesday && values.tuesday.value,
                    wednesday: values.wednesday && values.wednesday.value,
                    locationID: getState().routing.locationBeforeTransitions.query.id,
                    id: contactPointId
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Contact point successfully edited!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestLocationDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            console.log(error.message);
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);