import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { handleErrors } from './../utils';
import { addNotification } from './NotificationsActions';
import { requestCardDetails } from './CardActions';

export const requestNewCardPrefix = values => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_PREFIX_REQUESTED });
        return fetch(`${ API }/cardprefix`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cardPrefix: {
                    cardNumLength: values.cardNumLength,
                    issuedCardId: getState().routing.locationBeforeTransitions.query.id,
                    rangeFrom: values.rangeFrom,
                    rangeTo: values.rangeTo
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Card prefix successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestCardDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            console.log(error);
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestCardPrefixEdit = (values, cardPrefixId) => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/cardprefix/update`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                cardPrefix: {
                    id: cardPrefixId,
                    cardNumLength: values.cardNumLength,
                    issuedCardId: getState().routing.locationBeforeTransitions.query.id,
                    rangeFrom: values.rangeFrom,
                    rangeTo: values.rangeTo
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Card prefix successfully edited!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestCardDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);