import { API } from './../constants';
import * as actionTypes from './../actionTypes';
import { handleErrors } from './../utils';
import { addNotification } from './NotificationsActions';
import { requestIssuerDetails } from './IssuerActions';

export const requestNewLocation = values => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/location`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                location: {
                    countryCode: values.countryCode.value,
                    issuerId: getState().routing.locationBeforeTransitions.query.id,
                    locationType: values.locationType.value,
                    loss: values.loss.value,
                    obsolete: values.obsolete.value,
                    replacement: values.replacement.value,
                    sortCode: values.sortCode,
                    address: {
                        addressLineOne: values.addressLineOne,
                        addressLineTwo: values.addressLineTwo,
                        county: values.county,
                        town: values.town,
                        postcode: values.postcode
                    }
                }
            })
        }).then(handleErrors).then((json) => {
            dispatch(addNotification(
                'Location successfully added!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestIssuerDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            console.log(error);
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestLocationEdit = (values, locationId) => (
    (dispatch, getState) => {
        dispatch({ type: actionTypes.NEW_CARD_REQUESTED });
        return fetch(`${ API }/location/update`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                location: {
                    countryCode: values.countryCode.value,
                    issuerId: getState().routing.locationBeforeTransitions.query.id,
                    locationType: values.locationType.value,
                    loss: values.loss.value,
                    obsolete: values.obsolete.value,
                    replacement: values.replacement.value,
                    sortCode: values.sortCode,
                    id: locationId,
                    address: {
                        addressLineOne: values.addressLineOne,
                        addressLineTwo: values.addressLineTwo,
                        county: values.county,
                        town: values.town,
                        postcode: values.postcode
                    }
                }
            })
        }).then(handleErrors).then(() => {
            dispatch(addNotification(
                'Location successfully edited!',
                'success'
            ));
            dispatch({ type: actionTypes.MODAL_CLOSE });
            dispatch(requestLocationDetails(getState().routing.locationBeforeTransitions.query.id));
        }).catch(error => {
            dispatch(addNotification(
                error.message,
                'error'
            ));
        });
    }
);

export const requestLocationDetails = id => (
    dispatch => {
        dispatch({ type: actionTypes.LOCATION_DETAILS_REQUESTED });
        return fetch(`${ API }/location/id/${id}`)
            .then(handleErrors).then((json) => {
                dispatch({
                    type: actionTypes.LOCATION_DETAILS_RECEIVED,
                    location: json
                });
            }).catch(error => {
                dispatch(addNotification(
                    error.message,
                    'error'
                ));
            });
    }
);