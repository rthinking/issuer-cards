import * as actionTypes from './../actionTypes';

export const addNotification = (notificationMessage, notificationType = 'error') => (
    (dispatch, getState) => {
        if(getState().notifications) {
            clearTimeout(getState().notifications.timer);
        }
        let timer = setTimeout(() => {
            dispatch(removeNotification());
        }, 5000);
        dispatch({
            type: actionTypes.NOTIFICATION_ADDED,
            notificationMessage,
            notificationType,
            timer
        });
    }
);

export const removeNotification = () => (
    (dispatch, getState) => {
        let state = getState().notifications;
        if(state) {
            clearTimeout(state.timer);
        }
        dispatch({
            type: actionTypes.NOTIFICATION_REMOVED
        });
    }
);