import React from 'react';
import { Route, IndexRoute, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { Provider } from 'react-redux';

import { store } from './store/createStore';
import App from './containers/App/App.jsx';
import Home from './containers/HomePage/HomePage.jsx';
import IssuerPage from './containers/IssuerPage/IssuerPage.jsx';
import IssuedCardPage from './containers/IssuedCardPage/IssuedCardPage.jsx';
import LocationPage from './containers/LocationPage/LocationPage.jsx';

const history = syncHistoryWithStore(browserHistory, store);

export const NoMatch = () =>
{
	return (
		<div>
			404
		</div>
	);
};

export const routes = (
	<Provider store={ store }>
		<Router history={ history }>
            <Route component={ App } path="/">
				<IndexRoute component={ Home } />
				<Route component={ IssuerPage } path="issuer" />
				<Route component={ LocationPage } path="location" />
				<Route component={ IssuedCardPage } path="card" />
				<Route component={ NoMatch } path="*" />
			</Route>
		</Router>
	</Provider>
);